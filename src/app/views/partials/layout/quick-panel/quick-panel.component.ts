// Angular
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
// Layout
import { OffcanvasOptions } from '../../../../core/_base/layout';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'kt-quick-panel',
	templateUrl: './quick-panel.component.html',
	styleUrls: ['./quick-panel.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class QuickPanelComponent implements OnInit {

	// Public properties
	offcanvasOptions: OffcanvasOptions = {
		overlay: true,
		baseClass: 'kt-quick-panel',
		closeBy: 'kt_quick_panel_close_btn',
		toggleBy: 'kt_quick_panel_toggler_btn'
	};
	org_id: string = '';

	constructor(
		private route: ActivatedRoute
	) {}

	ngOnInit(): void {
		this.route.paramMap.subscribe(param => {console.log(param);
		});
	}
}
