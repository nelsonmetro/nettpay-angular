import { BusinessService, BusinessModel } from './../../../../core/nettpay';
import { AuthService, User } from './../../../../core/auth';
// Angular
import { Component, OnInit, ElementRef, ViewChild, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { tap, map, switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';
// Services and Models


@Component({
  selector: 'kt-payroll-list',
  templateUrl: './payroll-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [BusinessService]
})
export class PayrollListComponent implements OnInit, OnDestroy{
  private unsubscribe: Subject<any>;
  user: User;
	business: BusinessModel = new BusinessModel();
  // Table fields


  constructor(
    private router: Router,
    private auth: AuthService,
    private org: BusinessService
    ) {
    this.unsubscribe = new Subject();
  }
  // /**
  // * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
  // */

	// /**
	//  * On init
	//  */
  ngOnInit() {
    document.title = 'nettpay | Index';
  }
  getUser() {

  }
	/**
	 * On Destroy
	 */
  ngOnDestroy() {
  }

}
