import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PayrollListComponent } from './payroll-list/payroll-list.component';
import {
  MatButtonModule,
  MatMenuModule,
  MatSelectModule,
  MatInputModule,
  MatTableModule,
  MatAutocompleteModule,
  MatRadioModule,
  MatIconModule,
  MatNativeDateModule,
  MatProgressBarModule,
  MatDatepickerModule,
  MatCardModule,
  MatPaginatorModule,
  MatSortModule,
  MatCheckboxModule,
  MatProgressSpinnerModule,
  MatSnackBarModule,
  MatTabsModule,
  MatTooltipModule,
  MatDialogModule,
} from '@angular/material';
import { NgbProgressbarModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { PartialsModule } from '../../partials/partials.module';
import { PayrollComponent } from './payroll.component';

const routes: Routes = [
  {
    path: '',
    component: PayrollComponent,
    children: [
      {
        path: '',
        redirectTo: 'ls',
        pathMatch: 'full'
      },
      {
        path: 'ls',
        component: PayrollListComponent
      }
    ]
  }
];

@NgModule({
  declarations: [
    PayrollComponent,
    PayrollListComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    PartialsModule,
    RouterModule.forChild(routes),
    MatButtonModule,
    MatMenuModule,
    MatSelectModule,
    MatInputModule,
    MatTableModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatIconModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatDatepickerModule,
    MatCardModule,
    MatPaginatorModule,
    MatSortModule,
    MatCheckboxModule,
		MatProgressSpinnerModule,
		MatSnackBarModule,
		MatTabsModule,
		MatTooltipModule,
		NgbProgressbarModule,
    MatDialogModule,
    RouterModule,
		ReactiveFormsModule,
  ]
})
export class PayrollModule { }
