import { User, AuthService } from './../../../../core/auth';
import { BusinessModel, BusinessService } from './../../../../core/nettpay';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

export class Tax {
  user: User;
	business: BusinessModel = new BusinessModel();
  id: number;
  annual: number;
  monthly: number;
  rate: number;
  amount: number;
  constructor() {
    // this.monthly = this.amount = this.annual = this.rate = 0;
  }
}


@Component({
  selector: 'kt-tax-table',
  templateUrl: './tax-table.component.html',
  styleUrls: ['./tax-table.component.scss']
})
export class TaxTableComponent implements OnInit {
  taxtable: Tax[] = [];
  tax: Tax = new Tax();
  errorMessage: string;
  touched = false;

  @ViewChild('txtanual') txtAnual: ElementRef;

  constructor(
		private auth: AuthService,
		private org: BusinessService,
    private route: ActivatedRoute
    ) { }

  ngOnInit() {
    document.title = 'nettpay | Tax Table';
  }
  addToTable() {
    if (this.tax.annual && this.tax.rate) {
      this.touched = true;
      this.errorMessage = '';
    }
    if (!this.touched) {
      this.errorMessage = 'your input fields are empty';
      return;
    }
    this.tax.monthly = Math.round((this.tax.annual / 12) * 100) / 100;
    this.tax.amount = Math.round((this.tax.monthly * (this.tax.rate / 100)) * 100) / 100;
    this.taxtable.push(this.tax);
    this.tax = new Tax();
    this.txtAnual.nativeElement.focus();
  }
  resetErrors(event) {
    this.touched = false;
    this.errorMessage = '';
  }
}
