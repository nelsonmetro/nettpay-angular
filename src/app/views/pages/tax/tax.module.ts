import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaxComponent } from './tax.component';
import { Routes, RouterModule } from '@angular/router';
import {
  MatButtonModule,
  MatMenuModule,
  MatSelectModule,
  MatInputModule,
  MatTableModule,
  MatAutocompleteModule,
  MatRadioModule,
  MatIconModule,
  MatNativeDateModule,
  MatProgressBarModule,
  MatDatepickerModule,
  MatCardModule,
  MatPaginatorModule,
  MatSortModule,
  MatCheckboxModule,
  MatProgressSpinnerModule,
  MatSnackBarModule,
  MatTabsModule,
  MatTooltipModule,
  MatDialogModule,
} from '@angular/material';
import { NgbProgressbarModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PartialsModule } from '../../partials/partials.module';
import { TaxTableComponent } from './tax-table/tax-table.component';
import { RoundPipe } from '../../../pipes/round.pipe';

const routes: Routes = [
  {
    path: '',
    component: TaxComponent,
    children: [
      {
        path: '',
        redirectTo: 'ls',
        pathMatch: 'full'
      },
      {
        path: 'ls',
        component: TaxTableComponent
      }
    ]
  }
];

@NgModule({
  declarations: [
    TaxComponent,
    TaxTableComponent,
    RoundPipe
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    PartialsModule,
    RouterModule.forChild(routes),
    MatButtonModule,
    MatMenuModule,
    MatSelectModule,
    MatInputModule,
    MatTableModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatIconModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatDatepickerModule,
    MatCardModule,
    MatPaginatorModule,
    MatSortModule,
    MatCheckboxModule,
		MatProgressSpinnerModule,
		MatSnackBarModule,
		MatTabsModule,
		MatTooltipModule,
		NgbProgressbarModule,
    MatDialogModule,
    RouterModule,
    ReactiveFormsModule
  ]
})
export class TaxModule { }
