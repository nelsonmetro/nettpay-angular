import { BusinessModel, BusinessService } from './../../../../core/nettpay';
// Angular
import { Component, OnInit, ViewChild, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { MatPaginator, MatSnackBar } from '@angular/material';
// NGRX
import { Store } from '@ngrx/store';
// LODASH
import _ from 'lodash';
// Services
import { LayoutUtilsService} from '../../../../core/_base/crud';
// Models
import { User, AuthService } from '../../../../core/auth';
import { AppState } from '../../../../core/reducers';
import { ActivatedRoute } from '@angular/router';

// Components

// Table with EDIT item in MODAL
// ARTICLE for table with sort/filter/paginator
// https://blog.angular-university.io/angular-material-data-table/
// https://v5.material.angular.io/components/table/overview
// https://v5.material.angular.io/components/sort/overview
// https://v5.material.angular.io/components/table/overview#sorting
// https://www.youtube.com/watch?v=NSt9CI3BXv4
@Component({
	selector: 'kt-type',
	templateUrl: './type.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class TypeComponent implements OnInit, OnDestroy {
	user: User;
	business: BusinessModel = new BusinessModel();
	pTypes = [
		{ userId: 1, id: 1, title: 'Temp' },
		{ userId: 1, id: 2, title: 'Trainee' },
		{ userId: 1, id: 3, title: 'Constraint' },
		{ userId: 1, id: 4, title: 'Permant' }
	];
	count = this.pTypes.length;

	@ViewChild(MatPaginator) paginator: MatPaginator;
	/**
	 * Component constructor
	 *
	 * @param store: Store<AppState>
	 * @param dialog: MatDialog
     * @param snackBar: MatSnackBar
	 * @param layoutUtilsService: LayoutUtilsService
	 */
	constructor(
		private store: Store<AppState>,
		public snackBar: MatSnackBar,
		private layoutUtilsService: LayoutUtilsService,
		private auth: AuthService,
		private org: BusinessService,
		private route: ActivatedRoute
		) { }

	ngOnInit() {
		// If the user changes the sort order, reset back to the first page.
		document.title = 'nettpay | Package Types';
	}
	/**
	   * On Destroy
	   */
	ngOnDestroy() {
	}

	addType(input: HTMLInputElement) {
		let item = { userId: 1, id: ++this.count, title: input.value };
		input.value = null;
		this.pTypes.splice(0, 0, item);
	}
	deleteType(target) {
		let index = this.pTypes.indexOf(target);
		this.pTypes.splice(index, 1);
	}
}
