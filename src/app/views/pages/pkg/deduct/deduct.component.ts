import { User, AuthService } from './../../../../core/auth';
import { BusinessModel, BusinessService } from './../../../../core/nettpay';
// Angular
import { Component, OnInit, ElementRef, ViewChild, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
// Material
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
// RXJS
import { debounceTime, distinctUntilChanged, tap, skip, delay } from 'rxjs/operators';
import { fromEvent, merge, Observable, of, Subscription } from 'rxjs';
// NGRX
import { Store, select } from '@ngrx/store';
import { AppState } from '../../../../core/reducers';
// CRUD
import { LayoutUtilsService, MessageType, QueryParamsModel } from '../../../../core/_base/crud';
import { DeductEditDialogComponent } from './deduct-edit/deduct-edit.dialog.component';
import { TranslateService } from '@ngx-translate/core';

// Table with EDIT item in new page
// ARTICLE for table with sort/filter/paginator
// https://blog.angular-university.io/angular-material-data-table/
// https://v5.material.angular.io/components/table/overview
// https://v5.material.angular.io/components/sort/overview
// https://v5.material.angular.io/components/table/overview#sorting
// https://www.youtube.com/watch?v=NSt9CI3BXv4
@Component({
  selector: 'kt-deduct',
  templateUrl: './deduct.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeductComponent implements OnInit, OnDestroy {
  user: User;
	business: BusinessModel = new BusinessModel();

  private subscriptions: Subscription[] = [];

	/**
	 * Component constructor
	 *
	 * @param dialog: MatDialog
	 * @param activatedRoute: ActivatedRoute
	 * @param router: Router
	 * @param layoutUtilsService: LayoutUtilsService
	 * @param store: Store<AppState>
	 */
  constructor(public dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
    private layoutUtilsService: LayoutUtilsService,
    private store: Store<AppState>,
		private auth: AuthService,
		private org: BusinessService,
    private route: ActivatedRoute
    ) { }

  ngOnInit() {
    document.title = 'nettpay | Deductions';
  }

  /**
	 * On Destroy
	 */
  ngOnDestroy() {
    this.subscriptions.forEach(el => el.unsubscribe());
  }
}
