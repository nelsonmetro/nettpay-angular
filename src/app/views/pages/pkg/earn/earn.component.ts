import { BusinessModel, BusinessService } from './../../../../core/nettpay';
import { AfterViewInit, AfterViewChecked } from '@angular/core';
// Angular
import { Component, OnInit, ElementRef, ViewChild, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
// Material
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator, MatSort, MatSnackBar, MatDialog } from '@angular/material';
// RXJS
import { debounceTime, distinctUntilChanged, tap, skip, take, delay } from 'rxjs/operators';
import { fromEvent, merge, Observable, of, Subscription } from 'rxjs';
// LODASH
import { each, find } from 'lodash';
// NGRX
import { Store, select } from '@ngrx/store';
import { AppState } from '../../../../core/reducers';

// Services
import { LayoutUtilsService, MessageType, QueryParamsModel } from '../../../../core/_base/crud';
// Models
import {
	User,
	Role,
	UsersDataSource,
	UserDeleted,
	UsersPageRequested,
	selectUserById,
	selectAllRoles,
	AuthService
} from '../../../../core/auth';
import { EarnEditDialogComponent } from './earn-edit/earn-edit.dialog.component';
import { TranslateService } from '@ngx-translate/core';

// Table with EDIT item in MODAL
// ARTICLE for table with sort/filter/paginator
// https://blog.angular-university.io/angular-material-data-table/
// https://v5.material.angular.io/components/table/overview
// https://v5.material.angular.io/components/sort/overview
// https://v5.material.angular.io/components/table/overview#sorting
// https://www.youtube.com/watch?v=NSt9CI3BXv4
@Component({
  selector: 'kt-earn',
  templateUrl: './earn.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class EarnComponent implements OnInit, OnDestroy {
	user: User;
	business: BusinessModel = new BusinessModel();
	// Subscriptions
	private subscriptions: Subscription[] = [];

	constructor(
		public dialog: MatDialog,
		private store: Store<AppState>,
		private router: Router,
		private layoutUtilsService: LayoutUtilsService,
		private translate: TranslateService,
		private cdr: ChangeDetectorRef,
		private auth: AuthService,
		private org: BusinessService,
		private route: ActivatedRoute) {}

  ngOnInit() {
		document.title = 'nettpay | Earnings';
  }

	/**
	 * On Destroy
	 */
	ngOnDestroy() {
	}
}
