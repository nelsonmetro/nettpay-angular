import { User, AuthService } from './../../../../core/auth';
import { BusinessModel, BusinessService } from './../../../../core/nettpay';
import { Component, OnInit } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'kt-emp-edit',
  templateUrl: './emp-edit.component.html',
})
export class EmpEditComponent implements OnInit {
  user: User;
	business: BusinessModel = new BusinessModel();
  model: NgbDateStruct;
  date: {year: number, month: number};
  public isCollapsed = false;
  constructor(
		private auth: AuthService,
		private org: BusinessService,
    private route: ActivatedRoute
    ) { }

  ngOnInit() {
    document.title = 'nettpay | Employee';
  }

}
