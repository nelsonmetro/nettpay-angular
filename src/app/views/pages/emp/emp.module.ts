import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmpListComponent } from './emp-list/emp-list.component';
import { EmpEditComponent } from './emp-edit/emp-edit.component';
import {
  MatButtonModule,
  MatMenuModule,
  MatSelectModule,
  MatInputModule,
  MatTableModule,
  MatAutocompleteModule,
  MatRadioModule,
  MatIconModule,
  MatNativeDateModule,
  MatProgressBarModule,
  MatDatepickerModule,
  MatCardModule,
  MatPaginatorModule,
  MatSortModule,
  MatCheckboxModule,
  MatProgressSpinnerModule,
  MatSnackBarModule,
  MatTabsModule,
  MatTooltipModule,
  MatDialogModule,
} from '@angular/material';
import { NgbProgressbarModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { PartialsModule } from '../../partials/partials.module';
import { EmpComponent } from './emp.component';

const routes: Routes = [
  {
    path: '',
    component: EmpComponent,
    children: [
      {
        path: '',
        redirectTo: 'ls',
        pathMatch: 'full'
      },
      {
        path: 'ls',
        component: EmpListComponent
      },
      {
        path: 'add',
        component: EmpEditComponent
      },
      {
        path: 'add/:id',
        component: EmpEditComponent
      }
    ]
  }
];

@NgModule({
  declarations: [
    EmpComponent,
    EmpListComponent,
    EmpEditComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    PartialsModule,
    RouterModule.forChild(routes),
    MatButtonModule,
    MatMenuModule,
    MatSelectModule,
    MatInputModule,
    MatTableModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatIconModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatDatepickerModule,
    MatCardModule,
    MatPaginatorModule,
    MatSortModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatTabsModule,
    MatTooltipModule,
    NgbProgressbarModule,
    MatDialogModule,
    RouterModule,
    ReactiveFormsModule,
  ]
})
export class EmpModule { }
