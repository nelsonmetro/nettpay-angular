import { User, AuthService } from './../../../../core/auth';
import { BusinessModel, BusinessService } from './../../../../core/nettpay';
import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
// Services and Models
import { MatDialog, MatSnackBar } from '@angular/material';
import { Subscription } from 'rxjs';
// RXJS
import { LayoutUtilsService } from '../../../../core/_base/crud';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../core/reducers';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'kt-emp-list',
  templateUrl: './emp-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers:[BusinessService]
})
export class EmpListComponent implements OnInit, OnDestroy {
	user: User;
	business: BusinessModel = new BusinessModel();
  // Table fields
  // Subscriptions
  private subscriptions: Subscription[] = [];

   /**
	* Component constructor
	*
	* @param dialog: MatDialog
	* @param snackBar: MatSnackBar
	* @param layoutUtilsService: LayoutUtilsService
	* @param translate: TranslateService
	* @param store: Store<AppState>
	*/
  constructor(
	private auth: AuthService,
	private org: BusinessService,
	private route: ActivatedRoute
  ) { }

  /**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		document.title = 'nettpay | Employees';
	}

	/**
	 * On Destroy
	 */
	ngOnDestroy() {
		this.subscriptions.forEach(el => el.unsubscribe());
	}
	

}
