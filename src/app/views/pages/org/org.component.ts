// Angular
import { Component, ChangeDetectionStrategy, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
// RxJS
import { Observable } from 'rxjs';
// NGRX
import { Store, select } from '@ngrx/store';
// AppState
import { AppState } from '../../../core/reducers';
// Auth
import { Permission } from '../../../core/auth';

const userManagementPermissionId: number = 2;

@Component({
  selector: 'kt-org',
  templateUrl: './org.component.html'
})
export class OrgComponent{

  // hasUserAccess$: Observable<boolean>;
	currentUserPermission$: Observable<Permission[]>;

	/**
	   * Component constructor
	   *
	   * @param store: Store<AppState>
	   * @param router: Router
	   */
	constructor(private store: Store<AppState>, private router: Router) {
	}

}
