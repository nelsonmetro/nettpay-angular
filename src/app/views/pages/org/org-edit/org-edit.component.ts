import { BusinessBankingModel } from './../../../../core/nettpay/_models/business-banking.model';
import { TypesUtilsService } from './../../../../core/_base/crud/utils/types-utils.service';
import { BusinessModel } from './../../../../core/nettpay/_models/business.model';
import {
	BusinessService,
	BusinessDetailsModel,
	BusinessAddressModel
} from '../../../../core/nettpay';
import {
	AuthService,
	User
} from './../../../../core/auth';
import {
	Component,
	OnInit,
	OnDestroy
} from '@angular/core';
import {
	FormGroup,
	FormBuilder,
	Validators
} from '@angular/forms';
import {
	Subject
} from 'rxjs';
import {
	Router, ActivatedRoute
} from '@angular/router';
import {
	tap, map
} from 'rxjs/operators';

@Component({
	selector: 'kt-org-edit',
	templateUrl: './org-edit.component.html',
	providers: [BusinessService]
})
export class OrgEditComponent implements OnInit, OnDestroy {
	detailsForm: FormGroup;
	bankingForm: FormGroup;
	addressForm: FormGroup;

	loading = false;
	errors: any = [];
	monthend: Date[];
	user;
	business: BusinessModel = new BusinessModel();
	private returnUrl: any;

	private unsubscribe: Subject < any > ; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/

	constructor(
		private router: Router,
		private fb: FormBuilder,
		private auth: AuthService,
		private org: BusinessService,
		private route: ActivatedRoute,
		private typesUtilsService: TypesUtilsService
	) {
		this.unsubscribe = new Subject();
	}

	//#region init
	ngOnInit() {
		this.businessDataForm();
		document.title = 'nettpay | Business';

		this.route.paramMap.subscribe(param => {
			this.returnUrl = param['returnUrl'] || '/';
			if (param.keys.length < 1) {return;}
			this.initOrg(param.get('orgid'));
		});
		//  user session
		this.initUser();

	}

	ngOnDestroy(): void {
		this.unsubscribe.next();
		this.unsubscribe.complete();
		this.loading = false;
	}
	initUser() {
		this.auth.sessionInfo()
			.subscribe((res) => {
				this.user = res.result.user;
			});
	}
	initOrg(id: string) {
		this.org.getBusinessById(id)
		.subscribe((res: BusinessModel) => {console.log(res);
			this.business = res['result'];
			this.detailsForm.patchValue(res['result']);
			this.bankingForm.patchValue(res['result']);
			this.addressForm.patchValue(res['result']);
		});
	}

	businessDataForm() {
		this.detailsForm = this.fb.group({
			title: [this.business.title, Validators.compose([
				Validators.required,
				Validators.minLength(3),
				Validators.maxLength(100)
			])],
			webURL: [this.business.webURL, Validators.compose([
				Validators.minLength(5),
				Validators.maxLength(100)
			])],
			description: [this.business.description, Validators.compose([])],
			regNo: [this.business.regNo, Validators.compose([])],
			vatNo: [this.business.vatNo, Validators.compose([])],
			uifNo: [this.business.uifNo, Validators.compose([])],
			beeStatus: [this.business.beeStatus, Validators.compose([])],
			monthEnd: [this.business.monthEnd, Validators.compose([])],
			telephone: [this.business.telephone, Validators.compose([])]
		});
		this.bankingForm = this.fb.group({
			bankName: [this.business.bankName, Validators.compose([Validators.required])],
			accountNo: [this.business.accountNo, Validators.compose([Validators.required])],
			branchCode: [this.business.branchCode, Validators.compose([])],
			branchName: [this.business.branchName, Validators.compose([])]

		});
		this.addressForm = this.fb.group({
			addressLine1: [this.business.addressLine1, Validators.compose([Validators.required])],
			addressLine2: [this.business.addressLine2, Validators.compose([])],
			suburb: [this.business.suburb, Validators.compose([Validators.required])],
			zipCode: [this.business.zipCode, Validators.compose([])]
		});
	}
	//#endregion
	/**
	 * Returns page title
	 */
	getTitle(): string {
		if (this.business.id.length > 0) {
			return `Edit  <b>'${this.business.title}'</b>`;
		}

		return 'Define Business';
	}
//#region prepare
	prepareBusinessDetails(): BusinessDetailsModel {
		const controls = this.detailsForm.controls;
		const _business = new BusinessDetailsModel();
		_business.id = this.business.id;
		_business.title = controls['title'].value;
		_business.webURL = controls['webURL'].value;
		_business.description = controls['description'].value;
		_business.regNo = controls['regNo'].value;
		_business.vatNo = controls['vatNo'].value;
		_business.uifNo = controls['uifNo'].value;
		_business.beeStatus = controls['beeStatus'].value;
		// tslint:disable-next-line: radix
		_business.monthEnd = parseInt(controls['monthEnd'].value) || 0;
		_business.telephone = controls['telephone'].value;
		_business.ownerId = this.user.id;
		return _business;
	}

	prepareBankingDetails(): BusinessBankingModel {
		const controls = this.bankingForm.controls;
		const _business = new BusinessBankingModel();
		_business.id = this.business.id;
		_business.bankName = controls['bankName'].value;
		_business.accountNo = controls['accountNo'].value;
		_business.branchName = controls['branchName'].value;
		_business.branchCode = controls['branchCode'].value;
		return _business;
	}

	prepareAddressDetails(): BusinessAddressModel {
		const controls = this.addressForm.controls;
		const _business = new BusinessAddressModel();
		_business.id = this.business.id;
		_business.addressLine1 = controls['addressLine1'].value;
		_business.addressLine2 = controls['addressLine2'].value;
		_business.suburb = controls['suburb'].value;
		_business.zipCode = controls['zipCode'].value;
		return _business;
	}
	//#endregion

	//#region submit
	onSubmitDetails() {
		const controls = this.detailsForm.controls;
		// check form
		if (this.detailsForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}
		this.loading = true;
		const targetBusiness = this.prepareBusinessDetails();console.log(targetBusiness);
		if (targetBusiness.id.length > 0) {
			this.updateBusiness(targetBusiness);
		} else {
			this.createBusiness(targetBusiness);
		}
	}
	onSubmitBanking() {
		const controls = this.bankingForm.controls;
		// check form
		if (this.bankingForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}
		this.loading = true;
		const targetBusiness = this.prepareBankingDetails();console.log(targetBusiness);
		this.editBankningDetails(targetBusiness);
	}
	onSubmitAddress() {
		const controls = this.addressForm.controls;
		// check form
		if (this.addressForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}console.log(controls);
		this.loading = true;
		const targetBusiness = this.prepareAddressDetails();console.log(targetBusiness);
		this.editPhysicalAddress(targetBusiness);
	}

//#endregion
	createBusiness(_business: BusinessDetailsModel) {console.log(_business);console.log(this.user.id);
		this.org.createBusiness(_business)
		.subscribe((res) => {
			console.log(res);
			this.router.navigateByUrl(this.returnUrl); // Main page
		});
	}

	updateBusiness(_business: BusinessDetailsModel) {
		this.org.editBusinesssDetials(_business)
		.subscribe((res) => {
			console.log(res);
			this.router.navigateByUrl(this.returnUrl); // Main page
		});
	}

	editBankningDetails(_business: BusinessBankingModel) {
		this.org.editBusinesssBanking(_business)
		.subscribe((res) => {
			console.log(res);
			this.router.navigateByUrl(this.returnUrl); // Main page
		});
	}

	editPhysicalAddress(_business: BusinessAddressModel) {console.log(_business);
		this.org.editBusinesssAddress(_business)
		.subscribe((res) => {
			console.log(res);
			this.router.navigateByUrl(this.returnUrl); // Main page
		});
	}
}
