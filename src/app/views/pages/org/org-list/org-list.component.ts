import { AuthService, User } from './../../../../core/auth';
import { BusinessService, BusinessModel } from './../../../../core/nettpay';
import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'kt-org-list',
  templateUrl: './org-list.component.html'
})
export class OrgListComponent implements OnInit {
  private unsubscribe: Subject<any>;
  user: User;
  business: BusinessModel = new BusinessModel();
  businesses: BusinessModel[] = [];
  constructor(
    private router: Router,
    private auth: AuthService,
    private org: BusinessService
  ) {
    this.unsubscribe = new Subject();
   }

  ngOnInit() {console.log(window.localStorage);
    //  user session
    this.initUser();
    // user businesses
    this.initOrg();
  }
  initUser() {
    this.auth.sessionInfo()
    .subscribe((res) => {console.log(res);
      this.user = res.result.user;
    });
  }
  initOrg() {
    this.org.getUserBusinesses()
    .subscribe((res) => { console.log(res);
      this.businesses = res['result'].items;
    });
  }
}
