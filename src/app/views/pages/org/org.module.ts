import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatButtonModule,
  MatMenuModule,
  MatSelectModule,
  MatInputModule,
  MatTableModule,
  MatAutocompleteModule,
  MatRadioModule,
  MatIconModule,
  MatNativeDateModule,
  MatProgressBarModule,
  MatDatepickerModule,
  MatCardModule,
  MatPaginatorModule,
  MatSortModule,
  MatCheckboxModule,
  MatProgressSpinnerModule,
  MatSnackBarModule,
  MatTabsModule,
  MatTooltipModule,
  MatDialogModule,
} from '@angular/material';
import { NgbProgressbarModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { PartialsModule } from '../../partials/partials.module';
import { OrgListComponent } from './org-list/org-list.component';
import { OrgComponent } from './org.component';
import { OrgEditComponent } from './org-edit/org-edit.component';

const routes: Routes = [
  {
    path: '',
    component: OrgComponent,
    children: [
      {
        path: '',
        redirectTo: 'ls',
        pathMatch: 'full'
      },
      {
        path: 'ls',
        component: OrgListComponent
      },
      {
        path: 'add',
        component: OrgEditComponent
      },
      {
        path: 'edit/:orgid',
        component: OrgEditComponent
      }
    ]
  }
];

@NgModule({
  declarations: [
    OrgComponent,
    OrgListComponent,
    OrgEditComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    PartialsModule,
    RouterModule.forChild(routes),
    MatButtonModule,
    MatMenuModule,
    MatSelectModule,
    MatInputModule,
    MatTableModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatIconModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatDatepickerModule,
    MatCardModule,
    MatPaginatorModule,
    MatSortModule,
    MatCheckboxModule,
		MatProgressSpinnerModule,
		MatSnackBarModule,
		MatTabsModule,
		MatTooltipModule,
		NgbProgressbarModule,
    MatDialogModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class OrgModule { }
