// Angular
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// Components
import { BaseComponent } from './base/base.component';
import { ErrorPageComponent } from './content/error-page/error-page.component';
// Auth
import { AuthGuard } from '../../core/auth';

const routes: Routes = [
	{
		path: '',
		component: BaseComponent,
		canActivate: [AuthGuard],
		children: [
			{
				path: 'dash',
				loadChildren: 'app/views/pages/dash/dash.module#DashboardModule'
			},
			{
				path: 'org', // <= Page URL
				loadChildren: 'app/views/pages/org/org.module#OrgModule'
			},
			{
				path: 'emp', // <= Page URL
				loadChildren: 'app/views/pages/emp/emp.module#EmpModule'
			},
			{
				path: 'payroll', // <= Page URL
				loadChildren: 'app/views/pages/payroll/payroll.module#PayrollModule'
			},
			{
				path: 'tax', // <= Page URL
				loadChildren: 'app/views/pages/tax/tax.module#TaxModule'
			},
			{
				path: 'pkg',
				loadChildren: 'app/views/pages/pkg/pkg.module#PkgModule'
			},
			{ path: 'error/:type', component: ErrorPageComponent },
			{ path: '', redirectTo: 'org', pathMatch: 'full' },
			{ path: '**', redirectTo: 'org', pathMatch: 'full' }
		]
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class PagesRoutingModule {
}
