export class MenuConfig {
	public defaults: any = {
		aside: {
			self: {},
			items: [
				{
					title: 'Payroll',
					root: true,
					icon: 'flaticon2-dashboard',
					page: '/payroll',
					bullet: 'dot',
				},
				{
					title: 'Employees',
					root: true,
					icon: 'flaticon-users',
					page: '/emp', // <= URL
				},
				{
					title: 'Package',
					root: true,
					icon: 'flaticon2-soft-icons-1',
					submenu: [
						{
							title: 'Earnings',
							icon: 'flaticon-coins',
							page: '/pkg/earns'
						},
						{
							title: 'Deductions',
							icon: 'flaticon2-pie-chart',
							page: '/pkg/deducts'
						},
						{
							title: 'Types',
							icon: 'flaticon2-list-2',
							page: '/pkg/types'
						},
					]
				},
				{
					title: 'Report',
					root: true,
					icon: 'flaticon2-bar-chart',
					page: '/dash', // <= URL
				},
			]
		},
	};

	public get configs(): any {
		return this.defaults;
	}
}
