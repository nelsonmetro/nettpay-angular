import { BusinessModel } from '../_models/business.model';
import {
	BusinessDetailsModel
} from '../_models/business-details.model';
import {
	Injectable
} from '@angular/core';
import {
	HttpClient,
	HttpHeaders
} from '@angular/common/http';
import {
	HttpUtilsService
} from '../../_base/crud';
import {
	Observable
} from 'rxjs';
import { BusinessAddressModel } from '../_models/business-address.model';
import { BusinessBankingModel } from '../_models/business-banking.model';

const BASE_URL = 'http://127.0.0.1:21021';

@Injectable()
export class BusinessService {

	constructor(private http: HttpClient, private httpUtils: HttpUtilsService) {}

	// CREATE =>  POST: add a new business to the server
	createBusiness(business: BusinessDetailsModel): Observable < any > {
		const httpHeaders = new HttpHeaders();
		httpHeaders.set('Content-Type', 'application/json');
		return this.http.post < any > (`${BASE_URL}/api/services/app/Business/CreateAsync`, business, {
			headers: httpHeaders
		});
	}
	// READ
	getOwnerBusinesses(owner: number): Observable <BusinessModel[]> {
		return this.http.get <BusinessModel[]> (`${BASE_URL}/api/services/app/Business/GetListAsync?Id=${owner}`);
	}
	getUserBusinesses(): Observable<BusinessModel[]> {
		return this.http.get <BusinessModel[]> (`${BASE_URL}/api/services/app/Business/GetUserListAsync`);
	}

	getBusinessById(id: string): Observable<BusinessModel> {
		return this.http.get<BusinessModel>(`${BASE_URL}/api/services/app/Business/GetDetailAsync?Id=${id}`);
	}
	// UPDATE
	editBusinesssDetials(business: BusinessDetailsModel): Observable<any> {
		const httpHeaders = new HttpHeaders();
		httpHeaders.set('Content-Type', 'application/json');
		return this.http.put(`${BASE_URL}/api/services/app/Business/UpdateAsync`, business, { headers: httpHeaders});
	}
	editBusinesssBanking(business: BusinessBankingModel): Observable<any> {
		const httpHeaders = new HttpHeaders();
		httpHeaders.set('Content-Type', 'application/json');
		return this.http.post < any >(`${BASE_URL}/api/services/app/Business/EditBankingAsync`,business, { headers: httpHeaders});
	}
	editBusinesssAddress(business: BusinessAddressModel): Observable<any> {
		const httpHeaders = new HttpHeaders();
		httpHeaders.set('Content-Type', 'application/json');
		return this.http.post < any >(`${BASE_URL}/api/services/app/Business/EditAddressAsync`,business, { headers: httpHeaders});
	}
	// DELETE

}
