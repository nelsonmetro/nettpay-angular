import { BaseModel } from '../../_base/crud';
import { EmployeeModel } from './employee.model';
export class BusinessDetailsModel extends BaseModel {
    id: string;
    tenantId: number;
    regNo: string;
    title: string;
    description: string;
    telephone: string;
    webURL: string;
    monthEnd: number;
    payeNo: string;
    uifNo: string;
    vatNo: string;
    beeStatus: string;
    ownerId: number;
    
    _emps: EmployeeModel[];

    clear(ownerId: number) {
        this.regNo = '';
        this.title = '';
        this.description = '';
        this.telephone = '';
        this.webURL = '';
        this.monthEnd = 0;
        this.payeNo = '';
        this.uifNo = '';
        this.vatNo = '';
        this.beeStatus = '';
        this.ownerId = ownerId;
    }
}
