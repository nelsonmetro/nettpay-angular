import { BaseModel } from '../../_base/crud';
import { EmployeeModel } from './employee.model';
export class BusinessModel extends BaseModel {
    id: string;
    tenantId: number;
    regNo: string;
    title: string;
    description: string;
    addressLine1: string;
    addressLine2: string;
    suburb: string;
    zipCode: string;
    telephone: string;
    webURL: string;
    monthEnd: number;
    payeNo: string;
    uifNo: string;
    bankName: string;
    accountNo: string;
    branchName: string;
    branchCode: string;
    vatNo: string;
    beeStatus: string;
    ownerId: number;
    creationTime: Date;

    employeesCount: EmployeeModel[];

    constructor()
    {
        super();
        this.id = '';
    }

    clear(ownerId: number) {
        this.regNo = '';
        this.title = '';
        this.description = '';
        this.addressLine1 = '';
        this.addressLine2 = '';
        this.suburb = '';
        this.zipCode = '';
        this.telephone = '';
        this.webURL = '';
        this.monthEnd = 0;
        this.payeNo = '';
        this.uifNo = '';
        this.bankName = '';
        this.accountNo = '';
        this.branchName = '';
        this.branchCode = '';
        this.vatNo = '';
        this.beeStatus = '';
        this.ownerId = ownerId;
    }
}
