import { BaseModel } from '../../_base/crud';
export class BusinessAddressModel extends BaseModel {
    id: string;
    addressLine1: string;
    addressLine2: string;
    suburb: string;
    zipCode: string;

    clear(ownerId: number) {
        this.addressLine1 = '';
        this.addressLine2 = '';
        this.suburb = '';
        this.zipCode = '';
    }
}
