import { BaseModel } from '../../_base/crud';
import { EmployeeModel } from './employee.model';
export class BusinessBankingModel extends BaseModel {
    id: string;
    bankName: string;
    accountNo: string;
    branchName: string;
    branchCode: string;

    clear(ownerId: number) {
        this.bankName = '';
        this.accountNo = '';
        this.branchName = '';
        this.branchCode = '';
    }
}
