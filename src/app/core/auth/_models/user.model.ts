import { BaseModel } from '../../_base/crud';
import { Address } from './address.model';
import { SocialNetworks } from './social-networks.model';

export class User extends BaseModel {
    id: number;
    userName: string;
    password: string;
    emailAddress: string;
    accessToken: string;
    refreshToken: string;
    roles: number[];
    pic: string;
    name: string;
    surname: string;
    occupation: string;
	companyName: string;
	phoneNumber: string;
    address: Address;
    socialNetworks: SocialNetworks;

    clear(): void {
        this.id = undefined;
        this.userName = '';
        this.password = '';
        this.emailAddress = '';
        this.roles = [];
        this.name = '';
        this.surname = '';
        this.accessToken = 'access-token-' + Math.random();
        this.refreshToken = 'access-token-' + Math.random();
        this.pic = './assets/media/users/default.jpg';
        this.occupation = '';
        this.companyName = '';
        this.phoneNumber = '';
        this.address = new Address();
        this.address.clear();
        this.socialNetworks = new SocialNetworks();
        this.socialNetworks.clear();
    }
}
